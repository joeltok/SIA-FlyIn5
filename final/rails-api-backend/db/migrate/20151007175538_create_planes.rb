class CreatePlanes < ActiveRecord::Migration
  def change
    create_table :planes do |t|
    	t.string :price
      t.string :datetime_out
      t.string :datetime_land

      t.timestamps null: false
    end
  end
end
