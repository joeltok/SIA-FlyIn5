class LogicController < ApplicationController
	before_action :api_key

	def demo
		@create = HTTP.post('http://partners.api.skyscanner.net/apiservices/pricing/v1.0',
	 										  :form => {:apiKey => @apiKey,
										 			 			  :country => 'SG',
										 					 	 	:currency => 'SGD',
										 					 		:locale => 'en-GB',
										 					 		:originplace => 'SIN-sky',
										 					 		:destinationplace => 'LHR-sky',
										 					 		:outbounddate => '2015-11-21',
										 					 		#:inbounddate => '2015-10-26',
										 					 		:cabinclass => 'Business',
										 					 		:adults => '1'})
		
		@poll = HTTP.get("#{@create.headers['Location']}" + '?apiKey=' + @apiKey + '&includecarriers=SQ')

		hash = JSON.parse(@poll.body)
		itineraries_array = hash['Itineraries']
		price = itineraries_array[3]['PricingOptions'][1]['Price']
		outboundLegId = itineraries_array[0]['OutboundLegId']
		dateout  = outboundLegId[6..15]
		dateland = outboundLegId[29..38]


		Ship.first.update_attribute(:name, price.to_s + ' | ' + dateout + ' | ' + dateland + ' | ' + itineraries_array[0].to_s)
		@ship = Ship.first
		render json: @ship
	end

	def planes

#		params[:origin]
#		params[:destination]
#		params[:originDate]
#		params[:destinationDate]
#		params[:cabinClass]
		
		Plane.all.each {|p| p.destroy}

		@create = HTTP.post('http://partners.api.skyscanner.net/apiservices/pricing/v1.0',
	 										  :form => {:apiKey => @apiKey,
										 			 			  :country => 'SG',
										 					 	 	:currency => 'SGD',
										 					 		:locale => 'en-GB',
										 					 		:originplace => params[:origin],
										 					 		:destinationplace => params[:destination],
										 					 		:outbounddate => params[:originDate],
										 					 		#:inbounddate => params[:destinationDate],
										 					 		:cabinclass => params[:cabinClass],
										 					 		:adults => '1'})

#		Ship.first.update_attribute(:name, params[:origin] + ' | ' + params[:destination] + ' | ' + params[:originDate] + ' | ' + params[:cabinClass])
#		@ship = Ship.first
#		render json: @ship
		

		sleep(5)
		@poll = HTTP.get("#{@create.headers['Location']}" + '?apiKey=' + @apiKey + '&includecarriers=SQ')
		hash = JSON.parse(@poll.body)

		itineraries_array = hash['Itineraries']
		itineraries_array.each do |i|
			price = 10000
			i['PricingOptions'].each do |po|
				price = po['Price'] < price ? po['Price'] : price
			end
			datetime_out = i['OutboundLegId'][6..15]
			datetime_land = i['OutboundLegId'][29..38]
			Plane.create!(price: price.to_s,
										datetime_out: datetime_out,
										datetime_land: datetime_land)
		end
	
		@planes = Plane.all

		render json: @planes
	end

	def hotels

		render json: @hotels
	end

	private

	def api_key
		@apiKey = 'ah341157971439527307553061694277'
	end
end
