Rails.application.routes.draw do

  root 'logic#demo'
  get 'planes' => 'logic#planes'
  post 'hotels' => 'logic#hotels'

end
